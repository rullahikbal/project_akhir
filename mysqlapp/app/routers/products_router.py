from app import app
from app.controllers import products_controller
from flask import Blueprint, request

products_blueprint = Blueprint("products_router", __name__)


@app.route("/products", methods=["GET"])
def showProducts():
    return products_controller.shows()


@app.route("/product", methods=["GET"])
def showProduct():
    params = request.json
    return products_controller.show(params)


@app.route("/product/insert", methods=["POST"])
def insertProduct():
    params = request.json
    return products_controller.insert(params)


@app.route("/product/update", methods=["POST"])
def updateProduct():
    params = request.json
    return products_controller.update(params)


@app.route("/product/delete", methods=["POST"])
def deleteProduct():
    params = request.json
    return products_controller.delete(params)