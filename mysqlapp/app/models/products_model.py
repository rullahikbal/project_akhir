from mysql.connector import connect
class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='shophone',
                              user='root',
                              password='mysqlpass')
        except Exception as e:
            print(e)

    def showProducts(self):
        cursor = self.db.cursor()
        query = '''select * from products'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showProductById(self, params):
        cursor = self.db.cursor()
        query = '''
                select * 
                from products
                where product_id = {0};
            '''.format(params["product_id"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result


    def insertProduct(self, params):
        try:
            column = ', '.join(list(params.keys()))
            values = tuple(list(params.values()))
        
            crud_query = '''insert into products ({0}) values {1};'''.format(
                column, values)
            
            cursor = self.db.cursor()
            cursor.execute(crud_query)
            
            return 'success'

        except Exception as e:
            self.db.rollback()
            return str(e)

    def updateProductById(self, params):
        product_id = params['product_id']
        values = self.restructureParams(params)
        crud_query = '''update products set {0} where product_id = {1};'''.format(
            values, product_id)
        print(crud_query)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def deleteProductById(self, params):
        product_id = params['product_id']
        crud_query = '''delete from products where product_id = {0};'''.format(
            product_id)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, data):
        data.pop('product_id')
        list_data = ['{0} = "{1}"'.format(
            item[0], item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result