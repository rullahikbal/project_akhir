from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='shophone',
                              user='root',
                              password='mysqlpass')
        except Exception as e:
            print(e)

    def showUsers(self):
        cursor = self.db.cursor()
        query = '''select * from customers'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showUserById(self, params):
        cursor = self.db.cursor()
        query = '''
                select * 
                from customers
                where customer_id = {0};
            '''.format(params["customer_id"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def showUserByEmail(self, params):
        cursor = self.db.cursor()
        query = '''
                select *
                from customers
                where email = "{0}";
            '''.format(params["email"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result   

    def insertUser(self, params):
        try:
            column = ', '.join(list(params.keys()))
            values = tuple(list(params.values()))
        
            crud_query = '''insert into customers ({0}) values {1};'''.format(
                column, values)
            
            cursor = self.db.cursor()
            cursor.execute(crud_query)

        except Exception as e:
            print(e)

    def updateUserById(self, params):
        customer_id = params['customer_id']
        values = self.restructureParams(params)
        crud_query = '''update customers set {0} where customer_id = {1};'''.format(
            values, customer_id)
        print(crud_query)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def deleteUserById(self, params):
        customer_id = params['customer_id']
        crud_query = '''delete from customers where customer_id = {0};'''.format(
            customer_id)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, data):
        data.pop('customer_id')
        list_data = ['{0} = "{1}"'.format(
            item[0], item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
    

# if __name__ == "__main__":
#     mysqldb = database()
#     if mysqldb.db.is_connected():
#         print('Connected to MySQL database')
#         # mysqldb.showUsers()
#         # mysqldb.showUserById(data_id)
#         # mysqldb.showUserByEmail(data_email)
#         # mysqldb.insertUser(data_insert)
#         mysqldb.updateUserById(data_update)
#         # mysqldb.dataCommit()

#     if mysqldb.db is not None and mysqldb.db.is_connected():
#         mysqldb.db.close()