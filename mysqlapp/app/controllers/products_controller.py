from logging import error
from mysql.connector.connection import MySQLConnection
from mysql.connector.cursor import MySQLCursor
from app.models.products_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json
import datetime
import string

mysqldb = database()


def shows():
    dbresult = mysqldb.showProducts()
    result = []
    for items in dbresult:
        user = {
            "product_id": items[0],
            "product_name": items[1],
            "merk": items[2],
            "price": items[3],
            "stock": items[4]
        }
        result.append(user)

    return jsonify(result)


def show(params):
    dbresult = mysqldb.showProductById(params)
    product= {
            "product_id": dbresult[0],
            "product_name": dbresult[1],
            "merk": dbresult[2],
            "price": dbresult[3],
            "stock": dbresult[4]
        }
    return jsonify(product)


def insert(params):      
    for product_params in params:
    # for capitalize category
        product_params['merk'] = string.capwords(product_params['merk'])
        mysqldb.insertProduct(params)
        
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


def update(params):
    mysqldb.updateProductById(params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


def delete(params):
    mysqldb.deleteProductById(params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})
