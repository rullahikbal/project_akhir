from app.models.customers_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json
import datetime
import string

mysqldb = database()


def shows():
    dbresult = mysqldb.showUsers()
    result = []
    for items in dbresult:
        user = {
            "customer_id": items[0],
            "username": items[1],
            "fullname": items[2],
            "email": items[3],
            "created_at": items[4]
        }
        result.append(user)

    return jsonify(result)


def show(params):
    dbresult = mysqldb.showUserById(params)
    user = {
            "customer_id": dbresult[0],
            "username": dbresult[1],
            "fullname": dbresult[2],
            "email": dbresult[3],
            "created_at": dbresult[4]
        }
    return jsonify(user)


def insert(params):
    for customer_params in params:
        # for capitalize fullname
        customer_params['fullname'] = string.capwords(customer_params['fullname'])
        mysqldb.insertUser(customer_params)
        
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


def update(params):
    mysqldb.updateUserById(params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


def delete(params):
    mysqldb.deleteUserById(params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})

def token(params):
    dbresult = mysqldb.showUserByEmail(params)
    if dbresult is not None:
        # payload untuk JWT
        user = {
            "username": dbresult[1],
            "email": dbresult[3]
        }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(
            user, fresh=True, expires_delta=expires)

        data = {
            "data": user,
            "token_access": access_token
        }
    else:
        data = {
            "message": "Email tidak terdaftar"
        }
    return jsonify(data)