**Dokumentasi**

Berikut merupakan dokumentasi database untuk project akhir.
Berikut merupakan diagram ERD dari database
 
Berikut langkah dalam membuat database yang digunakan


•	Membuat database shophone
_create database shophone;_


•	Membuat table customers
_use shophone;_

_CREATE TABLE customers (
    customer_id INT AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    fullname VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (customer_id),
    UNIQUE KEY unique_username (username),
    UNIQUE KEY unique_email (email)
)  ENGINE=INNODB;_



•	Membuat table products
_use shophone;_

_CREATE TABLE products (
    product_id INT AUTO_INCREMENT,
    product_name VARCHAR(50) NULL,
    merk ENUM('Apple', 'Samsung', 'Xiaomi', 'Oppo', 'Vivo') NULL,
    price INT UNSIGNED NOT NULL DEFAULT 0,
    stock INT UNSIGNED NOT NULL DEFAULT 0,
    PRIMARY KEY (product_id)
)  ENGINE=INNODB;_


•	Membuat table orders
_use shophone;_

_CREATE TABLE orders (
    order_id int auto_increment,
    customer_id INT,
    product_id INT,
    quantity INT UNSIGNED NOT NULL DEFAULT 0,
    price INT UNSIGNED NOT NULL DEFAULT 0,
    total INT UNSIGNED NOT NULL DEFAULT 0,
    order_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (order_id),
    CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id)
        REFERENCES customers (customer_id),
    CONSTRAINT fk_orders_produtcs FOREIGN KEY (product_id)
        REFERENCES products (product_id)
)  ENGINE=INNODB;_


Berikut cara memakai API yang tersedia pada postman:


API Customers

•	Melihat seluruh user dengan method get
Parameter:
_localhost:5000/users_


•	Melihat user by id dengan method get
Parameter:
_localhost:5000/user_

Data:
_{
    "customer_id": 2
}_


•	Memasukkan user dengan method post
Parameter:
_localhost:5000/user/insert_

Data:
_[
    {
            "username": "sikono",
            "fullname": "sikono tama",
            "email": "tama@gmail.com"
},
            {
            "username": "baka",
            "fullname": "baka sata",
            "email": "sata@gmail.com"
    }
]_


•	Mengubah user by id dengan method post
Parameter:
_localhost:5000/user/update_

Data:
_{
    "username": "tower of",
    "email": "druaga@gmail.com",
    "customer_id": 2,
    "fullname": "tower of druaga"
    }_


•	Untuk menghapus user by id dengan method post
Parameter:
_localhost:5000/user/delete_

Data:
_{
    "user_id": 5
}_


•	Untuk meminta token user by email dengan method post
Parameter:
_localhost:5000/user/ requesttoken_

Data:
_{
    "email": "druaga@gmail.com"
}_


API Products


•	Melihat seluruh product dengan method get
Parameter:
_localhost:5000/products_


•	Melihat user by id dengan method get
Parameter:
_localhost:5000/product_

Data:
_{
    "product_id": 2
}_


•	Memasukkan product dengan method post
Parameter:
_localhost:5000/product/insert_

Data:
_[{
    "product_name": "Iphone 8",
    "merk": "apple",
    "price": 70000
},
{
    "product_name": "Xiaomi Note3",
    "merk": "xiaomi",
    "price": 6000,
    "stock": 40
}]_


•	Mengubah product by id dengan method post
Parameter:
_localhost:5000/product/update_

Data:
_{
    "product_id": 4,
    "product_name": "Siopo Limited Edition",
    "stock": 5
}_


•	Untuk menghapus product by id dengan method post
Parameter:
_localhost:5000/product/delete_

Data:
_{
    "product_id": 5
}_


**Note:
Untuk API Tabel Orders masih dalam tahap pengembangan**

